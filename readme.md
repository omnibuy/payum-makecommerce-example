# Payum MakeCommerce module by Omnibuy - usage example with Symfony

## Introduction
The purpose of this example project is to show how to use the `omnibuy/payum-makecommerce` module correctly with 
Symfony and templates. Since the MakeCommerce integration requires a credit card dialog, then a HTML form needs to 
be rendered and redirected using JavaScript.

## Usage

- `docker compose up`
- `docker compose exec fpm composer install`
- `docker compose exec fpm php bin/console doctrine:migrations:migrate`
- `docker compose exec fpm php bin/console server:start 0.0.0.0:8000`
- `ngrok http 8000`
- Add Ngrok HTTPS URL to MakeCommerce test account
- Open the Ngrok HTTPS URL in the browser.
- Use test cards found on [MakeCommerce page](https://makecommerce.net/for-developers/test-environment/)

## License

Project is released under the [MIT License](LICENSE).
